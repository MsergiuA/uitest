-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 28, 2019 at 10:03 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `testui`
--

-- --------------------------------------------------------

--
-- Table structure for table `usergrowth`
--

CREATE TABLE `usergrowth` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `earthYear` year(4) DEFAULT NULL,
  `age` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usergrowth`
--

INSERT INTO `usergrowth` (`id`, `user_id`, `earthYear`, `age`) VALUES
(1, 1, 2010, 20),
(2, 1, 2011, 21),
(3, 1, 2012, 22),
(4, 1, 2013, 23),
(5, 1, 2014, 24),
(6, 1, 2015, 25),
(7, 1, 2016, 26),
(8, 1, 2017, 27),
(9, 1, 2018, 28),
(10, 1, 2019, 29);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `birthdate` date NOT NULL,
  `createDate` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `birthdate`, `createDate`) VALUES
(1, 'Ionut', 'Ionut02', 'test@test.com', '2019-01-08', '2019-01-27 17:58:33'),
(2, 'Daniel', 'Daniel', 'daniel@test.com', '2019-01-07', '2019-01-27 17:58:56'),
(3, 'Andrei', 'Andrei', 'andrei@andrei.com', '2018-12-03', '2019-01-27 22:30:20'),
(4, 'Radu', 'Radu', 'telecom@telecom.com', '2011-11-06', '2019-01-27 22:05:38');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `usergrowth`
--
ALTER TABLE `usergrowth`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `usergrowth`
--
ALTER TABLE `usergrowth`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `usergrowth`
--
ALTER TABLE `usergrowth`
  ADD CONSTRAINT `usergrowth_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
