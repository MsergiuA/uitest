// DEFAULT CALL ON PAGE LOAD
$(document).ready(function(){
    ajaxCall(processAjaxResult, {'sortKey' : 'name','sortOrder' : 'asc'});
});


//MAIN AJAX CALL FUNCTION
function ajaxCall(callback, sortParams, extraParams){
    $.ajax({url: "test.php", success: function(result){
        callback(result, sortParams, extraParams);
    }});
}


// POPULATE TABLE WITH AJAX RESULT
function processAjaxResult(users, sortArray, extraParams)
{
    $(".mainTableBody").empty();
    var users = sortByKey(JSON.parse(users), sortArray['sortKey'], sortArray['sortOrder']);
    var tableRow = '';

    $(users).each(function (key) {
        tableRow = "<tr data-user-id=" + users[key]['id'] + " ondblclick='editRow(this)';" + " onclick='addHighChart(this)'>" +
                        "<td class='user-id'>" + users[key]['id'] +"</td>" +
                        "<td class='name'>" + users[key]['name'] +"</td>" +
                        "<td class='username'>" + users[key]['username'] +"</td>" +
                        "<td class='email'>" + users[key]['email'] +"</td>" +
                        "<td class='birthdate'>" + users[key]['birthdate'] +"</td>" +
                        "<td>" + users[key]['birthdate'] +"</td>" +
                    "</tr>";
        $('.mainTableBody').append(tableRow);
    });

    $("i").removeClass("selected");
    $('#' + extraParams +'').addClass('selected');

}


// RESIZE TABLE + POPULATE EDIT FORM
function editRow(row){
    resizeTableWidth();

    var className =  $('#mainTable').attr('class');


    if(className != 'col-md-6')
    {
        $('#formDiv').hide();
        $('#mainTable').css('height', '200px');
        return;
    }

    $('#formDiv').show();

    var userID = $(row).data( "user-id");
    ajaxCall(populateEditForm, {'sortKey' : 'id','sortOrder' : 'asc'}, userID);
}


// RESIZE TABLE ON DOUBLE CLICK FOR TR
function resizeTableWidth(){
    $('#mainTable').mousedown(function(e)
    {
        e.preventDefault();
    });
    $('#mainTable').toggleClass( "col-md-12 col-md-6");
    $('#formDiv').toggleClass( "col-md-0 col-md-6");
    $('#mainTable').css('font-size', '14px');
    $('#mainTable').css('height', '512px');
}


// POPULATE EDIT FORM
function populateEditForm(users, sortParams, userID){
    var searchedUser = getUserByIdFromArray(users, userID);

    $('#idInput').val(searchedUser['id']);
    $('#nameInput').val(searchedUser['name']);
    $('#usernameInput').val(searchedUser['username']);
    $('#emailInput').val(searchedUser['email']);
    $('#birthDateinput').val(searchedUser['birthdate']);
}

// REMOVE FORM ON CANCEL CLICK
function removeForm()
{
    $('#mainTable').toggleClass( "col-md-12 col-md-6");
    $('#formDiv').toggleClass( "col-md-0 col-md-6");
    $('#formDiv').hide();
    $('#mainTable').css('height', '200px');
}

// SORT FUNCTION
function sortByKey(array, key, order) {

    var sortedArray = array.sort(function(a, b) {
        var x = a[key];
        var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });


    if(order == 'desc')
    {
        sortedArray.reverse();
    }

    return sortedArray;
}

function addHighChart(user){

    var userID = $(user).data('user-id');

    $.ajax({url: "test.php", success: function(result){
        highChartDisplay(result, userID);
    }});

}

function getUserDataByID(userID)
{
    $.ajax({url: "userData.php", type: "GET", data: { user_id: userID },  success: function(data){
        highChartDisplayChart(data);
    }});
}


function getUserByIdFromArray(users, userID){

    var allusers = JSON.parse(users);
    var searchedUser = '';

    $(allusers).each(function (key) {
        if(allusers[key]['id'] == userID){
            searchedUser = allusers[key];
        }
    });

    return searchedUser;
}

function highChartDisplayChart(userDates)
{
    var userDates = JSON.parse(userDates);
    var ages = [];

    $(userDates).each(function (key) {
        ages.push(parseInt(userDates[key]['age']));
    });

    console.log(ages);

    Highcharts.chart('highChart', {

        title: {
            text: 'Age increase'
        },

        subtitle: {
            text: 'Source: thesolarfoundation.com'
        },

        xAxis: {
            title: {
                text: 'Earth Year'
            }
        },

        yAxis: {
            title: {
                text: 'User Age'
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },

        plotOptions: {
            series: {
                label: {
                    connectorAllowed: false
                },
                pointStart: 2010
            }
        },

        series: [{
            name: 'User Age',
            data: ages
        }],

        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }

    });
}

function highChartDisplay(users, userID){
    var searchedUser = getUserByIdFromArray(users, userID);
    getUserDataByID(userID);
}

