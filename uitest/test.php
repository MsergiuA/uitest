<?php

require_once('config.php');

$users = [];

$conenction = mysqli_connect($config['host'], $config['user'], $config['password'], $config['database']);

if (mysqli_connect_errno())
{
    echo "Failed to connect to database: " . mysqli_connect_error();
}

$query = 'SELECT *  FROM users';

$result = mysqli_query($conenction,$query);

if ($result->num_rows)
{
    while ($row=mysqli_fetch_assoc($result))
    {
        $users[] = $row;
    }
}

mysqli_close($conenction);

die(json_encode($users, true));